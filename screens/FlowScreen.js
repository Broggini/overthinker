import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native'


const FlowScreen = ({ navigation, route }) => {

    const { flow } = route.params;

    const zeroBlock = {"text": "STOP. OVERTHINKING."}
    
    const [overthinkingMessage, setOverthinkingMessage] = useState(true)
    const [currentBlock, setCurrentBlock] = useState(zeroBlock)
        
    useEffect(() => {
        setTimeout(()=>{
            setCurrentBlock(flow["1"])
            setOverthinkingMessage(false)
        }, 1000)
    }, [])

    const answer = (ans) => {
        setCurrentBlock(flow[currentBlock[ans]])
    }

    return (
        <View style={styles.container}>
            <View style={styles.bigTextContainer}>
                <Text style={styles.bigText}>{currentBlock.text}</Text>
            </View>
            
            {!overthinkingMessage &&
                <View>
                {!currentBlock.stop ? 
                    <View>
                        <TouchableOpacity style={styles.yesButton} onPress={() => answer('yes')}>
                            <Text style={styles.buttonText}>YES</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.noButton} onPress={() => answer('no')}>
                            <Text style={styles.buttonText}>NO</Text>
                        </TouchableOpacity>
                    </View>
                :
                
                    <View>
                        <TouchableOpacity style={styles.homeButton} onPress={() => navigation.navigate('Home')}>
                            <Text style={styles.buttonText}>Thank you.</Text>
                        </TouchableOpacity>
                    </View>
                }
                </View>
            }
            
        </View>
    )
}

export default FlowScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        paddingHorizontal: 10
    },

    bigTextContainer: {
        margin: "auto",
        padding: 20
    },
    bigText: {
        fontSize: 60,
        lineHeight: 80
    },

    yesButton: {
        alignItems: "center",
        backgroundColor: "#11A58A",
        padding: 10
    },
    noButton: {
        alignItems: "center",
        backgroundColor: "#EB665B",
        padding: 10
    },

    buttonText: {
        color: "white",
        fontSize: 30,
        lineHeight: 80

    },

    homeButton: {
        alignItems: "center",
        backgroundColor: "gray",
        padding: 10
    },
})

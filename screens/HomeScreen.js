import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native'

import lampFlow from '../flows/lampDoesntWorkFlow.json'
import buyFlow from '../flows/toBuyOrNotToBuyFlow.json'

const HomeScreen = ({ navigation }) => {

    const flows = [
        {
          "title": "Lamp doesn't work",
          "flowData": lampFlow
        },
        {
            "title": "Should you buy it?",
            "flowData": buyFlow
        }
      ]

    return (
        <View style={styles.container}>
            <View style={styles.title}>
                <Text style={styles.bigText}>Over<Text style={{ fontFamily: 'Iowan Old Style' }}>thinker.</Text></Text>
            </View>
            {
                flows.map((flow, i)=>(
                    <TouchableOpacity key={i} style={styles.button} onPress={() => navigation.navigate('Flow', { "flow": flow.flowData })}>        
                        <Text>{flow.title}</Text>
                    </TouchableOpacity>
                ))
            }
                        
        </View>
    )
}

export default HomeScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        paddingHorizontal: 10
    },
    title: {
        margin: "auto",
        padding: 20,
    },
    bigText: {
        fontSize: 60,
        lineHeight: 80
    },
    button: {
      alignItems: "center",
      backgroundColor: "#DDDDDD",
      padding: 10,
      margin: 10
    },
})
